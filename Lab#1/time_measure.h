#pragma once
#include <Windows.h>
#include <iostream>
#include <ctime>
#include <vector>
#include <limits>

using namespace std;

class TimeMeasure {
	vector<vector<double>> ticks;
	__int64 freq;
	__int64 tmpStart;
	int maxSize;

public:
	//n - number of blocks
	//m - number of measures for every block
	TimeMeasure(int n, int m, int timeModifier) : maxSize(m) {
		for (int i = 0; i < n; ++i)
		{
			vector<double> tmpVec;
			ticks.push_back(tmpVec);
		}

		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		freq /= timeModifier;

		SetThreadAffinityMask(GetCurrentThread(), 1);
		SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
	}

	~TimeMeasure()
	{
		int blockNum = 0;
		for (vector<vector<double>>::iterator it = ticks.begin(); it != ticks.end(); it++)
		{
			cout << "Block #" << blockNum << endl;
			double avg = 0;
			for (vector<double>::iterator inner_it = it->begin(); inner_it != it->end(); inner_it++)
			{
				cout << *inner_it << "  ";
				avg += *inner_it;
			}

			cout << "\nAvarage time: " << avg / it->size() << endl << endl;
			++blockNum;
		}
	}

	void Start()
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&tmpStart);
	}

	void Save(int n)
	{
		if (ticks[n].size() >= maxSize) return;

		__int64 end;
		QueryPerformanceCounter((LARGE_INTEGER*)&end);
		double newTime = (double)(end - tmpStart) / freq;
		ticks[n].push_back(newTime);
	}
};
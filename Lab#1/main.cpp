#include <iostream>
#include <vector>
#include <iomanip>
#include <ctime>
#include "time_measure.h"

using namespace std;

void TranspotMatrix(const int Size, vector<vector<int>> &Matrix)
{
    TimeMeasure tm(1, 1, 1000);
    /*Block 0*/
	tm.Start();
    for(int i = 0; i < Size; ++i)
    {
        for(int j = i; j < Size; ++j)
        {
            int buff;
            buff = Matrix[i][j];
            Matrix[i][j] = Matrix[j][i];
            Matrix[j][i] = buff;
        }
    }
    tm.Save(0);
}

void PrintMatrix(const int Size, const vector<vector<int>> &Matrix)
{
    for(int i = 0; i < Size; ++i)
    {
        for(int j = 0; j < Size; ++j)
        {
            cout << left << setw(3) << Matrix[i][j] << " ";
        }
        cout << endl;
    }
}

int main()
{
    srand(time(0));
    const int Size = 1000;

    vector<vector<int>> Matrix, Matrix1;

    for(int i = 0; i < Size; ++i)
    {
        Matrix.push_back({});
        for(int j = 0; j < Size; ++j)
        {
            Matrix[i].push_back((1 + rand() % 100));
        }
    }

    Matrix1 = Matrix;

    for (int i = 0; i < 10; i++)
	{
        TranspotMatrix(Size, Matrix);
        cout << "Avg time displayed in miliseconds" << endl;
        cout << "---------------------------------------------------" << endl;
        Matrix = Matrix1;
    }

    return 0;
}